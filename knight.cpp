#ifndef KNIGHT_CPP
#define KNIGHT_CPP

#include "chess.h"
#include "field.h"
#include "knight.h"

Knight::Knight(Chess * chess)
{
    this->chess = chess;
}

Knight::~Knight()
{

}

void Knight::turn(Field *field)
{
    // 8 possible fields

    // 2 up, 1 left
    if(field->gridRow-2>0 && field->gridColumn-1>0 && (this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1)->figureType==0 ||this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1));
        }
    }

    // 2 up, 1 right
    if(field->gridRow-2>0 && field->gridColumn+1<9 && (this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1)->figureType==0 ||this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1));
        }
    }

    // 1 up, 2 right
    if(field->gridRow-1>0 && field->gridColumn+2<9 && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2)->figureType==0 ||this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2));
        }
    }

    // 1 down, 2 right
    if(field->gridRow+1<9 && field->gridColumn+2<9 && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2)->figureType==0 ||this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2));
        }
    }

    // 2 down, 1 right
    if(field->gridRow+2<9 && field->gridColumn+1<9 && (this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1)->figureType==0 ||this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1));
        }
    }

    // 2 down, 1 left
    if(field->gridRow+2<9 && field->gridColumn-1>0 && (this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1)->figureType==0 ||this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1));
        }
    }

    // 1 down, 2 left
    if(field->gridRow+1<9 && field->gridColumn-2>0 && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2)->figureType==0 ||this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2));
        }
    }

    // 1 up, 2 left
    if(field->gridRow-1>0 && field->gridColumn-2>0 && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2)->figureType==0 ||this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2)->isWhite!=field->isWhite))
    {
        if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2))))
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2));
        }
        else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2))))
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2));
        }
    }

}

void Knight::threat(Field *field)
{
    // 8 possible fields

    // 2 up, 1 left
    if(field->gridRow-2>0 && field->gridColumn-1>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow-2, field->gridColumn-1)->isThreatendForWhite=true;
        }
    }

    // 2 up, 1 right
    if(field->gridRow-2>0 && field->gridColumn+1<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow-2, field->gridColumn+1)->isThreatendForWhite=true;
        }
    }

    // 1 up, 2 right
    if(field->gridRow-1>0 && field->gridColumn+2<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow-1, field->gridColumn+2)->isThreatendForWhite=true;
        }
    }

    // 1 down, 2 right
    if(field->gridRow+1<9 && field->gridColumn+2<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow+1, field->gridColumn+2)->isThreatendForWhite=true;
        }
    }

    // 2 down, 1 right
    if(field->gridRow+2<9 && field->gridColumn+1<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow+2, field->gridColumn+1)->isThreatendForWhite=true;
        }
    }

    // 2 down, 1 left
    if(field->gridRow+2<9 && field->gridColumn-1>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow+2, field->gridColumn-1)->isThreatendForWhite=true;
        }
    }

    // 1 down, 2 left
    if(field->gridRow+1<9 && field->gridColumn-2>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow+1, field->gridColumn-2)->isThreatendForWhite=true;
        }
    }

    // 1 up, 2 left
    if(field->gridRow-1>0 && field->gridColumn-2>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2)->isThreatendForBlack=true;
        }
        else
        {
            this->chess->getFieldAt(field->gridRow-1, field->gridColumn-2)->isThreatendForWhite=true;
        }
    }
}

#endif
