#ifndef BISHOP_H
#define BISHOP_H

class Field;
class Chess;

class Bishop
{
public:
    Bishop(Chess * chess);
    ~Bishop();

    Chess * chess;

    void turn(Field * field);
    void threat(Field * field);
};

#endif // BISHOP_H
