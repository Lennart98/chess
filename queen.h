#ifndef QUEEN_H
#define QUEEN_H

class Chess;
class Field;

class Queen
{
public:
    Queen(Chess * chess);
    ~Queen();

    Chess * chess;

    void turn(Field * field);
    void threat(Field * field);
};

#endif // QUEEN_H
