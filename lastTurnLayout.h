#ifndef LASTTURNLAYOUT_H
#define LASTTURNLAYOUT_H

#include <QGridLayout>
#include <QObject>
#include <QLabel>
#include <QSpacerItem>

class Chess;

class LastTurnLayout : public QGridLayout
{
    Q_OBJECT
public:
    LastTurnLayout(Chess * chess);
    ~LastTurnLayout();

    Chess * chess;

    QLabel * lastTurnHeading;
    QLabel * lastTurn;
    QSpacerItem * bottomSpace;

};

#endif // LASTTURNLAYOUT_H
