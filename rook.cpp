#ifndef ROOK_CPP
#define ROOK_CPP

#include "rook.h"
#include "chess.h"
#include "field.h"


Rook::Rook(Chess * chess)
{
    this->chess = chess;
}

Rook::~Rook()
{

}

void Rook::turn(Field *field)
{
    //mark fields in all 4 directions until first figure
    int i;

    //up
    i = 1;
    while(field->gridRow-i>0)
    {
        if(this->chess->getFieldAt(field->gridRow-i, field->gridColumn)->figureType!=0 && this->chess->getFieldAt(field->gridRow-i, field->gridColumn)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow-i, field->gridColumn)->figureType!=0 && this->chess->getFieldAt(field->gridRow-i, field->gridColumn)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn));
            }
            break;
        }
        else {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn));
            }
             i++;
        }
    }

    //down
    i = 1;
    while(field->gridRow+i<9)
    {
        if(this->chess->getFieldAt(field->gridRow+i, field->gridColumn)->figureType!=0 && this->chess->getFieldAt(field->gridRow+i, field->gridColumn)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow+i, field->gridColumn)->figureType!=0 && this->chess->getFieldAt(field->gridRow+i, field->gridColumn)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn));
            }
            break;
        }
        else {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn));
            }
            i++;
        }
    }

    //left
    i = 1;
    while(field->gridColumn-i>0)
    {
        if(this->chess->getFieldAt(field->gridRow, field->gridColumn-i)->figureType!=0 && this->chess->getFieldAt(field->gridRow, field->gridColumn-i)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow, field->gridColumn-i)->figureType!=0 && this->chess->getFieldAt(field->gridRow, field->gridColumn-i)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow, field->gridColumn-i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn-i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow, field->gridColumn-i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn-i));
            }
            break;
        }
        else {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow, field->gridColumn-i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn-i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow, field->gridColumn-i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn-i));
            }
            i++;
        }
    }

    //right
    i = 1;
    while(field->gridColumn+i<9)
    {
        if(this->chess->getFieldAt(field->gridRow, field->gridColumn+i)->figureType!=0 && this->chess->getFieldAt(field->gridRow, field->gridColumn+i)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow, field->gridColumn+i)->figureType!=0 && this->chess->getFieldAt(field->gridRow, field->gridColumn+i)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow, field->gridColumn+i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn+i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow, field->gridColumn+i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn+i));
            }
            break;
        }
        else {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow, field->gridColumn+i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn+i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow, field->gridColumn+i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn+i));
            }
            i++;
        }
    }
}

void Rook::threat(Field *field)
{
    Field * checkfield = nullptr;

    //mark fields in all 4 directions until first figure
    int i;

    //up
    i = 1;
    while(field->gridRow-i>0)
    {
        checkfield = this->chess->getFieldAt(field->gridRow-i, field->gridColumn);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else {
             if(field->isWhite==true)
             {
                 this->chess->getFieldAt(field->gridRow-i, field->gridColumn)->isThreatendForBlack=true;
             }
             else
             {
                 this->chess->getFieldAt(field->gridRow-i, field->gridColumn)->isThreatendForWhite=true;
             }
             i++;
        }
    }

    //down
    i = 1;
    while(field->gridRow+i<9)
    {
        checkfield = this->chess->getFieldAt(field->gridRow+i, field->gridColumn);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else {
             if(field->isWhite==true)
             {
                 this->chess->getFieldAt(field->gridRow+i, field->gridColumn)->isThreatendForBlack=true;
             }
             else
             {
                 this->chess->getFieldAt(field->gridRow+i, field->gridColumn)->isThreatendForWhite=true;
             }
             i++;
        }
    }


    //left
    i = 1;
    while(field->gridColumn-i>0)
    {
        checkfield = this->chess->getFieldAt(field->gridRow, field->gridColumn-i);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else {
             if(field->isWhite==true)
             {
                 this->chess->getFieldAt(field->gridRow, field->gridColumn-i)->isThreatendForBlack=true;
             }
             else
             {
                 this->chess->getFieldAt(field->gridRow, field->gridColumn-i)->isThreatendForWhite=true;
             }
             i++;
        }
    }


    //right
    i = 1;
    while(field->gridColumn+i<9)
    {
        checkfield = this->chess->getFieldAt(field->gridRow, field->gridColumn+i);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else {
             if(field->isWhite==true)
             {
                 this->chess->getFieldAt(field->gridRow, field->gridColumn+i)->isThreatendForBlack=true;
             }
             else
             {
                 this->chess->getFieldAt(field->gridRow, field->gridColumn+i)->isThreatendForWhite=true;
             }
             i++;
        }
    }
}

#endif
