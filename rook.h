#ifndef ROOK_H
#define ROOK_H

class Chess;
class Field;

class Rook
{
public:
    Rook(Chess * chess);
    ~Rook();

    Chess * chess;

    void turn(Field * field);
    void threat(Field * field);

};

#endif // ROOK_H
