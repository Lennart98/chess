#ifndef TURNINFORMATIONLAYOUT_H
#define TURNINFORMATIONLAYOUT_H

#include <QGridLayout>
#include <QLabel>
#include <QRadioButton>
#include <QButtonGroup>
#include <QSpacerItem>

class Chess;

class TurnInformationLayout : public QGridLayout
{
Q_OBJECT
public:
    TurnInformationLayout(Chess * chess);
    ~TurnInformationLayout();
    Chess * chess;
    QLabel * turnLabel;
    QRadioButton * isWhitesTurnRadioButton;
    QRadioButton * isBlacksTurnRadioButton;
    QButtonGroup * turnButtonGroup;

    QSpacerItem * bottomSpace;
  public slots:
    void toggleWhite();
    void toggleBlack();
};


#endif // TURNINFORMATIONLAYOUT_H
