#ifndef RECEIVER_H
#define RECEIVER_H

#include <QDialog>
#include <QString>
#include <QMessageBox>

class Chess;
class Field;
class PawnChangeDialog;
class Receiver
{
public:
    Receiver(Chess * chess);
    ~Receiver();
    PawnChangeDialog * pawnChangeDialog;

    Chess * chess;

    void receive(Field * field);
    void changePawn(Field * field);

    QString getTurnString(Field * oldField, Field * newField);
    QString getCoordinates(Field * field);
};

#endif // RECEIVER_H
