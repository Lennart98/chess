#ifndef CHESS_H
#define CHESS_H

#include <QMainWindow>
#include <QGridLayout>
#include <QLabel>
#include <QApplication>
#include <QDesktopWidget>


#include "field.h"
#include "fieldButton.h"
#include "pawn.h"
#include "rook.h"
#include "bishop.h"
#include "queen.h"
#include "knight.h"
#include "king.h"
#include "receiver.h"
#include "defeatedFigureGridLayout.h"
#include "turnInformationLayout.h"
#include "pawnChangeLayout.h"
#include "lastTurnLayout.h"

QT_BEGIN_NAMESPACE
// namespace Ui { class Chess; }
QT_END_NAMESPACE

class Chess : public QMainWindow
{
    Q_OBJECT

public:
    //constant Attributes
    int fontSize = QApplication::desktop()->geometry().width() / 48;
    int gridSize = QApplication::desktop()->geometry().width() / 48;
    int labelSize = fontSize / 2;

    //constructor + desctructur
    Chess(QWidget *parent = nullptr);
    ~Chess();

    // class attributes
    QList<FieldButton *> * fieldButtonList;
    QList<Field *> * fieldList;
    QList<QLabel *> *numberLabelList;
    QList<QLabel *> *letterLabelList;

    QWidget * app;
    QGridLayout * mainLayout;
    QGridLayout * chessGridLayout;
    QGridLayout * numberLabelGridLayout;
    QGridLayout * letterLabelGridLayout;


    DefeatedFigureGridLayout * defeatedBlackFiguresGridLayout;
    DefeatedFigureGridLayout * defeatedWhiteFiguresGridLayout;
    QGridLayout * controlPanelLayout;
    TurnInformationLayout * turnInformationGridLayout;
    LastTurnLayout * lastTurnLayout;
    PawnChangeLayout * pawnChangeLayout;


    bool enPasset = false;
    Field * enPassetField = nullptr;

    bool shortWhiteCastling = true;
    bool longWhiteCastling = true;
    bool shortBlackCastling = true;
    bool longBlackCastling = true;
    Field * castlingfield = nullptr;

    //Instances of Figures (for logic)
    Pawn * pawn;
    Rook * rook;
    Bishop * bishop;
    Queen * queen;
    King * king;
    Knight * knight;
    Receiver * receiver;

    bool isWhitesTurn = true;
    Field *clickedField;
    //class methods
    Field * getFieldAt(int,int);
    void initialiseGrid();
    void updateGrid();
    void initialiseLabels();
    void fieldClicked(int, int);
    void unselectGrid();
    void selectField(Field *);

    void threaten();
    void unthreaten();

    bool isWhiteCheck();
    bool isBlackCheck();

    bool isValidTurnForBlack(Field * start, Field * destination);
    bool isValidTurnForWhite(Field * start, Field * destination);

};
#endif // CHESS_H
