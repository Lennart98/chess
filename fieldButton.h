#ifndef FIELDBUTTON_H
#define FIELDBUTTON_H

#include <QPushButton>
#include <QWidget>

class Chess;
class FieldButton : public QPushButton
{
    Q_OBJECT

public:
    FieldButton(Chess * chess);
    Chess *chess;
    int gridRow;
    int gridColumn;
    ~FieldButton();

public slots:
    void click();
};

#endif // FIELDBUTTON_H
