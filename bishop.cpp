#ifndef BISHOP_CPP
#define BISHOP_CPP

#include "bishop.h"
#include "chess.h"
#include "field.h"

#include <iostream>


Bishop::Bishop(Chess * chess)
{
    this->chess = chess;
}

Bishop::~Bishop()
{

}

void Bishop::turn(Field *field)
{
    //mark fields in all 4 diagonal-directions until first figure
    int i;

    //up-left
    i = 1;
    while(field->gridRow-i>0 && field->gridColumn-i>0)
    {
        if(this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i)->figureType!=0 && this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i)->figureType!=0 && this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i));
            }
            break;
        }
        else
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i));
            }
            if (field->gridRow-i>0 && field->gridColumn-i>0) i++;
        }
     }

    //up-right
    i = 1;
    while(field->gridRow-i>0 && field->gridColumn+i<9)
    {
        if(this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i)->figureType!=0 && this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i)->figureType!=0 && this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i));
            }
            break;
        }
        else
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i));
            }
            if (field->gridRow-i>0 && field->gridColumn+i<9) i++;
        }
     }

    //down-left
    i = 1;
    while(field->gridRow+i<9 && field->gridColumn-i>0)
    {
        if(this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i)->figureType!=0 && this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i)->figureType!=0 && this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i));
            }
            break;
        }
        else
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i));
            }
            if (field->gridRow+i<9 && field->gridColumn-i>0) i++;
        }
    }

    //down-right
    i = 1;
    while(field->gridRow+i<9 && field->gridColumn+i<9)
    {
        if(this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i)->figureType!=0 && this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i)->isWhite==field->isWhite)
        {
            break;
        }
        else if (this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i)->figureType!=0 && this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i)->isWhite!=field->isWhite)
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i));
            }
            break;
        }
        else
        {
            if(field->isWhite && (this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i))))
            {
               this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i));
            }
            else if(field->isWhite==false && (this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i))))
            {
                this->chess->selectField(this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i));
            }
            if (field->gridRow+i<9 && field->gridColumn+i<9) i++;
        }
    }

}

void Bishop::threat(Field *field)
{
    //mark fields in all 4 diagonal-directions until first figure
    int i;

    Field * checkfield = nullptr;

    //up-left
    i = 1;
    while(field->gridRow-i>0 && field->gridColumn-i>0)
    {
        checkfield = this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else
        {
            if(field->isWhite==true)
            {
                this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i)->isThreatendForBlack=true;
            }
            else
            {
                this->chess->getFieldAt(field->gridRow-i, field->gridColumn-i)->isThreatendForWhite=true;
            }
            i++;
        }
     }

    //up-right
    i = 1;
    while(field->gridRow-i>0 && field->gridColumn+i<9)
    {
        checkfield = this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else
        {
            if(field->isWhite==true)
            {
                this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i)->isThreatendForBlack=true;
            }
            else
            {
                this->chess->getFieldAt(field->gridRow-i, field->gridColumn+i)->isThreatendForWhite=true;
            }
            i++;
        }
     }

    //down-left
    i = 1;
    while(field->gridRow+i<9 && field->gridColumn-i>0)
    {
        checkfield = this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else
        {
            if(field->isWhite==true)
            {
                this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i)->isThreatendForBlack=true;
            }
            else
            {
                this->chess->getFieldAt(field->gridRow+i, field->gridColumn-i)->isThreatendForWhite=true;
            }
            i++;
        }
     }

    //down-right
    i = 1;
    while(field->gridRow+i<9 && field->gridColumn+i<9)
    {
        checkfield = this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i);
        if((0<checkfield->figureType && checkfield->figureType<6) || (checkfield->figureType==6 && field->isWhite==checkfield->isWhite))
        {
            break;
        }
        else
        {
            if(field->isWhite==true)
            {
                this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i)->isThreatendForBlack=true;
            }
            else
            {
                this->chess->getFieldAt(field->gridRow+i, field->gridColumn+i)->isThreatendForWhite=true;
            }
            i++;
        }
     }

}


#endif
