#ifndef FIELD_H
#define FIELD_H

#include <QObject>

class FieldButton;
class Chess;
class Field
{
public:
    Field(Chess* chess, int, int);
    ~Field();

    int gridRow;
    int gridColumn;

    int figureType;     /*
                         * type = 0 -> leer
                         * type = 1 -> Bauer
                         * type = 2 -> Turm
                         * type = 3 -> Pferd
                         * type = 4 -> Laeufer
                         * type = 5 -> Dame
                         * type = 6 -> Koenig
                         */

    bool isWhite;
    bool isSelected = false;

    FieldButton * assosiatedFieldButton;
    Chess * chess;
    void update();

    bool isThreatendForWhite;
    bool isThreatendForBlack;
 };

#endif // FIELD_H
