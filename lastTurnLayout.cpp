#ifndef LASTTURNLAYOUT_CPP
#define LASTTURNLAYOUT_CPP

#include "chess.h"
#include "lastTurnLayout.h"

LastTurnLayout::LastTurnLayout(Chess * chess)
{
    this->chess = chess;

    QString textSize = "font-size: " + QString::number(this->chess->fontSize / 3) + "px;";
    this->lastTurnHeading = new QLabel("Letzter Zug:");
    this->lastTurnHeading->setStyleSheet(textSize);
    this->addWidget(this->lastTurnHeading, 0,1);

    this->lastTurn = new QLabel("");
    this->lastTurn->setStyleSheet(textSize);
    this->addWidget(this->lastTurn, 1 ,1);

    this->bottomSpace = new QSpacerItem(0, this->chess->gridSize/2);
    this->addItem(this->bottomSpace, 2,1);
}

LastTurnLayout::~LastTurnLayout()
{
    delete this->lastTurnHeading;
    delete this->lastTurn;
    this->removeItem(this->bottomSpace);
    delete this->bottomSpace;
}

#endif
