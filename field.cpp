#ifndef FIELD_CPP
#define FIELD_CPP

#include <field.h>
#include <fieldButton.h>
#include <chess.h>

/*
 * type = 0 -> leer
 * type = 1 -> Bauer
 * type = 2 -> Turm
 * type = 3 -> Pferd
 * type = 4 -> Laeufer
 * type = 5 -> Dame
 * type = 6 -> Koenig
 */

Field::Field(Chess * chess, int gridRow, int gridColumn)
{
    this->chess = chess;
    this->gridRow = gridRow;
    this->gridColumn = gridColumn;
    this->figureType = 0;
    this->isThreatendForBlack = false;
    this->isThreatendForWhite = false;
}

Field::~Field()
{

}

//redraw assosiated Button
void Field::update()
{
    if(this->figureType==0)
    {
        this->assosiatedFieldButton->setText("");
    }
    else if (this->figureType==1)
    {
        if(this->isWhite==true) this->assosiatedFieldButton->setText("\u2659");
        else this->assosiatedFieldButton->setText("\u265F");
    }
    else if (this->figureType==2)
    {
        if(this->isWhite==true) this->assosiatedFieldButton->setText("\u2656");
        else this->assosiatedFieldButton->setText("\u265C");
    }
    else if (this->figureType==3)
    {
        if(this->isWhite==true) this->assosiatedFieldButton->setText("\u2658");
        else this->assosiatedFieldButton->setText("\u265E");
    }
    else if (this->figureType==4)
    {
        if(this->isWhite==true) this->assosiatedFieldButton->setText("\u2657");
        else this->assosiatedFieldButton->setText("\u265D");
    }
    else if (this->figureType==5)
    {
        if(this->isWhite==true) this->assosiatedFieldButton->setText("\u2655");
        else this->assosiatedFieldButton->setText("\u265B");
    }
    else if (this->figureType==6)
    {
        if(this->isWhite==true) this->assosiatedFieldButton->setText("\u2654");
        else this->assosiatedFieldButton->setText("\u265A");
    }
}


#endif // FIELD_CPP
