#ifndef PAWN_H
#define PAWN_H

class Chess;
class Field;

class Pawn
{
public:
    Pawn(Chess * chess);
    Chess * chess;

    void turn(Field *field);
    void threat(Field *field);

    ~Pawn();
};

#endif // PAWN_H
