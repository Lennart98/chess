#ifndef PAWN_CPP
#define PAWN_CPP

#include <pawn.h>
#include <chess.h>
#include <field.h>
#include <iostream>
Pawn::Pawn(Chess * chess)
{
    this->chess = chess;
}

Pawn::~Pawn()
{

}

void Pawn::turn(Field * field)
{
    // normal turns
    if(field->isWhite)
    {
        // pawn on startRow
        if(field->gridRow==7)
        {
            if(this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->figureType==0)
            {
                if(this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn)))
                {
                   this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn));
                }
            }
            if(this->chess->getFieldAt(field->gridRow-2, field->gridColumn)->figureType==0 && this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->figureType==0)
            {
                if(this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-2, field->gridColumn)))
                {
                   this->chess->selectField(this->chess->getFieldAt(field->gridRow-2, field->gridColumn));
                }
            }
        }
        // pawn not on startRow
        if (1 < field->gridRow && field->gridRow < 8)
        {
            if(this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->figureType==0)
            {
                if(this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn)))
                {
                   this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn));
                }
            }
        }

    }
    else
    {
        if(field->gridRow==2)
        {
            if(this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->figureType==0)
            {
               if(this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridColumn)))
                {
                    this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn));
                }
            }
            if(this->chess->getFieldAt(field->gridRow+2, field->gridColumn)->figureType==0 && this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->figureType==0)
            {
                if(this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+2, field->gridColumn)))
                {
                    this->chess->selectField(this->chess->getFieldAt(field->gridRow+2, field->gridColumn));
                }
            }
        }
        else if (1 < field->gridRow && field->gridRow < 8)
        {
            if(this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->figureType==0)
            {
                if(this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridColumn)))
                {
                    this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn));
                }
            }
        }
    }


    // defeat enemy figures
    if(field->isWhite)
    {
        if(1<field->gridRow && field->gridRow < 8)
        {

            if((field->gridColumn > 1) && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->figureType!=0) && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->isWhite==false))
            {
                if(this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)))
                {
                   this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1));
                }
            }
            if((field->gridColumn < 8) && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->figureType!=0) && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->isWhite==false))
            {
                if(this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)))
                {
                   this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1));
                }
            }
        }
    }
    else
    {
        if(1<field->gridRow && field->gridRow < 8)
        {
            if((field->gridColumn > 1) && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->figureType!=0) && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->isWhite==true))
            {
                if(this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridRow-1)))
                {
                    this->chess->selectField((this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)));
                }
            }
            if((field->gridColumn < 8) && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->figureType!=0) && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->isWhite==true))
            {
                if(this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridRow+1)))
                {
                    this->chess->selectField((this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)));
                }
            }
        }
    }

    // en PassetFields
    if(this->chess->enPasset==true  && this->chess->enPassetField!=nullptr)
    {
        if(field->isWhite)
        {
            if(1<field->gridRow && field->gridRow < 8)
            {

                if((field->gridColumn > 1) && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)==this->chess->enPassetField))
                {
                    if(this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)))
                    {
                       this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1));
                    }
                }
                if((field->gridColumn < 8) && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)==this->chess->enPassetField))
                {
                    if(this->chess->isValidTurnForWhite(field, this->chess->getFieldAt(field->gridRow-1, field->gridColumn+11)))
                    {
                       this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1));
                    }
                }
            }
        }
        else
        {
            if(1<field->gridRow && field->gridRow < 8)
            {
                if((field->gridColumn > 1) && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)==this->chess->enPassetField))
                {
                    if(this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridRow-1)))
                    {
                        this->chess->selectField((this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)));
                    }
                }
                if((field->gridColumn < 8) && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)==this->chess->enPassetField))
                {
                    if(this->chess->isValidTurnForBlack(field, this->chess->getFieldAt(field->gridRow+1, field->gridRow+1)))
                    {
                        this->chess->selectField((this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)));
                    }
                }
            }
        }

    }
}

void Pawn::threat(Field *field)
{
    if(field->isWhite)
    {
        if(1<field->gridRow && field->gridRow < 8)
        {

            if((field->gridColumn > 1))
            {
                this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->isThreatendForBlack = true;
            }
            if((field->gridColumn < 8))
            {
                this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->isThreatendForBlack = true;
            }
        }
    }
    else
    {
        if(1<field->gridRow && field->gridRow < 8)
        {
            if((field->gridColumn > 1))
            {
              this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->isThreatendForWhite = true;
            }
            if((field->gridColumn < 8))
            {
              this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->isThreatendForWhite = true;
            }
        }
    }
}

#endif
