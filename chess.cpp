#ifndef CHESS_CPP
#define CHESS_CPP

#include "chess.h"
#include "fieldButton.h"

#include <QObject>
#include <QLabel>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QSpacerItem>

#include <iostream>

Chess::~Chess()
{
    // delete list-elements
    for(int i = 0; i<64; i++)
    {
        delete this->fieldButtonList->at(i);
        delete this->fieldList->at(i);
    }
    for(int i = 0; i<8; i++) {
        delete this->numberLabelList->at(i);
        delete this->letterLabelList->at(i);
    }

    //delete lists
    delete this->fieldButtonList;
    delete this->fieldList;
    delete this->numberLabelList;
    delete this->letterLabelList;

    //delete layouts
    delete this->numberLabelGridLayout;
    delete this->letterLabelGridLayout;
    delete this->chessGridLayout;   
    delete this->defeatedBlackFiguresGridLayout;
    delete this->defeatedWhiteFiguresGridLayout;
    delete this->turnInformationGridLayout;
    delete this->lastTurnLayout;
    delete this->pawnChangeLayout;
    delete this->mainLayout;

    delete this->app;

    // delete figure-instances
    delete this->pawn;
    delete this->rook;
    delete this->bishop;
    delete this->queen;
    delete this->king;
    delete this->knight;
    delete this->receiver;

}


Chess::Chess(QWidget *parent) : QMainWindow(parent)
{
    /* initialise Class-Attributes */
    this->fieldButtonList = new QList<FieldButton *>();
    this->fieldList = new QList<Field *>();
    this->numberLabelList = new QList<QLabel *>;
    this->letterLabelList = new QList<QLabel *>;

    this->app = new QWidget();
    this->mainLayout = new QGridLayout();
    this->chessGridLayout = new QGridLayout();
    this->numberLabelGridLayout = new QGridLayout();
    this->letterLabelGridLayout = new QGridLayout();

    // initialise receiver + figure-turn-classes
    this->receiver = new Receiver(this);
    this->pawn = new Pawn(this);
    this->rook = new Rook(this);
    this->bishop = new Bishop(this);
    this->queen = new Queen(this);
    this->king = new King(this);
    this->knight = new Knight(this);


    /* display chessGrid + labeling + turnInformation*/
    this->initialiseLabels();
    this->initialiseGrid();
    this->defeatedWhiteFiguresGridLayout = new DefeatedFigureGridLayout(this);
    this->mainLayout->addLayout(this->defeatedWhiteFiguresGridLayout, 0, 0, Qt::AlignTop);
    this->mainLayout->addLayout(this->numberLabelGridLayout, 0, 1);
    this->mainLayout->addLayout(this->chessGridLayout, 0, 2); //ggf anpassen
    this->mainLayout->addLayout(this->letterLabelGridLayout, 1, 2);
    this->turnInformationGridLayout = new TurnInformationLayout(this);
    this->controlPanelLayout = new QGridLayout();
    this->controlPanelLayout->addLayout(this->turnInformationGridLayout, 0, 0);
    this->defeatedBlackFiguresGridLayout = new DefeatedFigureGridLayout(this);
    this->mainLayout->addLayout(this->defeatedBlackFiguresGridLayout, 0, 3, Qt::AlignTop);

    this->lastTurnLayout = new LastTurnLayout(this);
    this->controlPanelLayout->addLayout(this->lastTurnLayout, 1, 0);

    this->pawnChangeLayout = new PawnChangeLayout(this);
    this->controlPanelLayout->addLayout(this->pawnChangeLayout, 2, 0);

    this->mainLayout->addLayout(this->controlPanelLayout, 0, 4, Qt::AlignTop);

    this->app->setLayout(this->mainLayout);
    this->setCentralWidget(this->app);
    //this->setMaximumSize(this->minimumSize());
}

Field * Chess::getFieldAt(int gridRow, int gridColumn)
{
    Field *actualField;
    for(int i = 0; i < 64; i++)
    {
       actualField = this->fieldList->at(i);
       if (actualField->gridRow==gridRow && actualField->gridColumn==gridColumn)
       {
           return actualField;
       }
    }
   return nullptr;
}

void Chess::fieldClicked(int gridRow, int gridColumn)
{
    Field *clickedField = this->getFieldAt(gridRow, gridColumn);
    if(clickedField->isSelected == true) {
        this->receiver->receive(clickedField);
    }
    else if(this->isWhitesTurn == clickedField->isWhite && clickedField->figureType==1 && this->clickedField!=clickedField)
    {
        this->unselectGrid();
        this->clickedField = clickedField;
        this->pawn->turn(clickedField);
    }
    else if(this->isWhitesTurn == clickedField->isWhite && clickedField->figureType==2 && this->clickedField!=clickedField)
    {
        this->unselectGrid();
        this->clickedField = clickedField;
        this->rook->turn(clickedField);
    }
    else if(this->isWhitesTurn == clickedField->isWhite && clickedField->figureType==3 && this->clickedField!=clickedField)
    {
        this->unselectGrid();
        this->clickedField = clickedField;
        this->knight->turn(clickedField);
    }
    else if(this->isWhitesTurn == clickedField->isWhite && clickedField->figureType==4 && this->clickedField!=clickedField)
    {
        this->unselectGrid();
        this->clickedField = clickedField;
        this->bishop->turn(clickedField);
    }
    else if(this->isWhitesTurn == clickedField->isWhite && clickedField->figureType==5 && this->clickedField!=clickedField)
    {
        this->unselectGrid();
        this->clickedField = clickedField;
        this->queen->turn(clickedField);
    }
    else if(this->isWhitesTurn == clickedField->isWhite && clickedField->figureType==6 && this->clickedField!=clickedField)
    {
        this->unselectGrid();
        this->clickedField = clickedField;
        this->king->turn(clickedField);
    }
    else
    {
        this->unselectGrid();
        this->clickedField = nullptr;
    }
}

void Chess::unselectGrid()
{
    for(int i=0; i<64; i++)
    {
        Field * actualField = this->fieldList->at(i);
        // normal setting
        QString fieldButtonStyle = "padding:0px; border-radius: 0px; font-size: " + QString::number(this->fontSize) + "px;";
        // background color
        if(((actualField->gridColumn+actualField->gridRow)%2) == 0)
        {
            fieldButtonStyle = fieldButtonStyle + "background-color: #edb99a;";
        }
        else
        {
            fieldButtonStyle = fieldButtonStyle + "background-color: #954D03;";
        }
        actualField->assosiatedFieldButton->setStyleSheet(fieldButtonStyle);
        actualField->isSelected = false;
    }

}

void Chess::selectField(Field * selectedField)
{
    // normal setting
    QString fieldButtonStyle = "padding:0px; border-radius: 0px; font-size: " + QString::number(this->fontSize) + "px;";
    // border
    fieldButtonStyle = fieldButtonStyle + "border-width: 5px; border-style: solid; border-color: #3d210e; border-radius: 5px;";
    // background color
    if(((selectedField->gridColumn+selectedField->gridRow)%2) == 0)
    {
        fieldButtonStyle = fieldButtonStyle + "background-color: #edb99a;";
    }
    else
    {
        fieldButtonStyle = fieldButtonStyle + "background-color: #954D03;";
    }
    selectedField->assosiatedFieldButton->setStyleSheet(fieldButtonStyle);
    selectedField->isSelected=true;
}

void Chess::initialiseGrid() {
    /*
     * initialise buttons + add them to fieldButtonList and chessGrid
     * initialise fields + add thems to fieldList
     */
    int i = 0;
    int gridColumn = 1;
    int gridRow = 1;
    QString fieldButtonStyle;
    for(; i<64; i++)
    {
        FieldButton *actualButton = new FieldButton(this);
        //actualButton->setText(QString::number(i));
        actualButton->setMinimumSize(this->gridSize,this->gridSize);
        actualButton->setMaximumSize(actualButton->minimumSize());
        actualButton->gridRow = gridRow;
        actualButton->gridColumn = gridColumn;

        fieldButtonStyle = "padding:0px; border-radius: 0px; font-size: " + QString::number(this->fontSize) + "px;";

        if(((gridColumn+gridRow)%2) == 0)
        {
            fieldButtonStyle = fieldButtonStyle + "background-color: #edb99a;";
        }
        else
        {
            fieldButtonStyle = fieldButtonStyle + "background-color: #954D03;";
        }

        actualButton->setStyleSheet(fieldButtonStyle);

        this->fieldButtonList->append(actualButton);
        this->chessGridLayout->addWidget(actualButton, gridRow, gridColumn, Qt::Alignment());

        Field * actualField = new Field(this, gridRow, gridColumn);
        actualField->assosiatedFieldButton = actualButton;
        this->fieldList->append(actualField);

        gridColumn = gridColumn + 1;
        if(gridColumn==9)
        {
           gridColumn = 1;
           gridRow = gridRow + 1;
        }
    }
    this->chessGridLayout->setSpacing(0);
    this->numberLabelGridLayout->setSpacing(0);
    this->letterLabelGridLayout->setSpacing(0);
    this->mainLayout->setSpacing(0);




    // black figures
    for(int i = 1; i<9; i++) {
        this->getFieldAt(1,i)->isWhite=false;
    }
    this->getFieldAt(1,1)->figureType=2;
    this->getFieldAt(1,2)->figureType=3;
    this->getFieldAt(1,3)->figureType=4;
    this->getFieldAt(1,4)->figureType=5;
    this->getFieldAt(1,5)->figureType=6;
    this->getFieldAt(1,6)->figureType=4;
    this->getFieldAt(1,7)->figureType=3;
    this->getFieldAt(1,8)->figureType=2;
    for(int i = 1; i<9; i++) {
        this->getFieldAt(2,i)->isWhite=false;
        this->getFieldAt(2,i)->figureType=1;
    }

    //white figures
    for(int i = 1; i<9; i++) {
        this->getFieldAt(7,i)->isWhite=true;
        this->getFieldAt(7,i)->figureType=1;
    }
    for(int i = 1; i<9; i++) {
        this->getFieldAt(8,i)->isWhite=true;
    }
    this->getFieldAt(8,1)->figureType=2;
    this->getFieldAt(8,2)->figureType=3;
    this->getFieldAt(8,3)->figureType=4;
    this->getFieldAt(8,4)->figureType=5;
    this->getFieldAt(8,5)->figureType=6;
    this->getFieldAt(8,6)->figureType=4;
    this->getFieldAt(8,7)->figureType=3;
    this->getFieldAt(8,8)->figureType=2;
    this->updateGrid();
}

void Chess::updateGrid() {
    for( int i = 0; i<64; i++)
    {
        this->fieldList->at(i)->update();
    }
}

void Chess::initialiseLabels()
{
    QLabel *number8 = new QLabel("8");
    this->numberLabelList->append(number8);
    QLabel *number7 = new QLabel("7");
    this->numberLabelList->append(number7);
    QLabel *number6 = new QLabel("6");
    this->numberLabelList->append(number6);
    QLabel *number5 = new QLabel("5");
    this->numberLabelList->append(number5);
    QLabel *number4 = new QLabel("4");
    this->numberLabelList->append(number4);
    QLabel *number3 = new QLabel("3");
    this->numberLabelList->append(number3);
    QLabel *number2 = new QLabel("2");
    this->numberLabelList->append(number2);
    QLabel *number1 = new QLabel("1");
    this->numberLabelList->append(number1);

    for(int i = 0; i<8; i++) {
        QLabel *actualNumberLabel = this->numberLabelList->at(i);
        actualNumberLabel->setStyleSheet("font-size: " + QString::number(this->labelSize) + "px;");
        actualNumberLabel->setMinimumSize(this->labelSize, this->gridSize);
        actualNumberLabel->setMaximumSize(actualNumberLabel->minimumSize());
        actualNumberLabel->setAlignment(Qt::AlignCenter);
        this->numberLabelGridLayout->addWidget(actualNumberLabel,i+1,0,Qt::Alignment());
    }

    QLabel *letterA = new QLabel("A");
    this->letterLabelList->append(letterA);
    QLabel *letterB = new QLabel("B");
    this->letterLabelList->append(letterB);
    QLabel *letterC = new QLabel("C");
    this->letterLabelList->append(letterC);
    QLabel *letterD = new QLabel("D");
    this->letterLabelList->append(letterD);
    QLabel *letterE = new QLabel("E");
    this->letterLabelList->append(letterE);
    QLabel *letterF = new QLabel("F");
    this->letterLabelList->append(letterF);
    QLabel *letterG = new QLabel("G");
    this->letterLabelList->append(letterG);
    QLabel *letterH = new QLabel("H");
    this->letterLabelList->append(letterH);

    for(int i = 0; i<8; i++) {
        QLabel *actualLetterLabel = this->letterLabelList->at(i);
        actualLetterLabel->setStyleSheet("font-size: " + QString::number(this->labelSize) + "px;");
        actualLetterLabel->setMinimumSize(this->gridSize, this->labelSize);
        actualLetterLabel->setMaximumSize(actualLetterLabel->minimumSize());
        actualLetterLabel->setAlignment(Qt::AlignCenter);
        this->letterLabelGridLayout->addWidget(actualLetterLabel,0,i+1,Qt::Alignment());
    }
}

void Chess::threaten()
{
    this->unthreaten();
    for(int row=1; row<9; row++)
    {
        for(int column=1; column<9; column++)
        {
            if(this->getFieldAt(row, column)->figureType==1)
            {
                this->pawn->threat(this->getFieldAt(row, column));
            }
            else if(this->getFieldAt(row, column)->figureType==2)
            {
                this->rook->threat(this->getFieldAt(row, column));
            }
            else if(this->getFieldAt(row, column)->figureType==3)
            {
                this->knight->threat(this->getFieldAt(row, column));
            }
            else if(this->getFieldAt(row, column)->figureType==4)
            {
                this->bishop->threat(this->getFieldAt(row, column));
            }
            else if(this->getFieldAt(row, column)->figureType==5)
            {
                this->queen->threat(this->getFieldAt(row, column));
            }
            else if(this->getFieldAt(row, column)->figureType==6)
            {
                this->king->threat(this->getFieldAt(row, column));
            }
        }
    }
}
void Chess::unthreaten()
{
    for(int row=1; row<9; row++)
    {
        for(int column=1; column<9; column++)
        {
            this->getFieldAt(row, column)->isThreatendForBlack = false;
            this->getFieldAt(row, column)->isThreatendForWhite = false;

        }
    }
}

bool Chess::isWhiteCheck()
{
    this->threaten();
    for(int row=1; row<9; row++)
    {
        for(int column=1; column<9; column++)
        {
            if(this->getFieldAt(row, column)->figureType==6 && this->getFieldAt(row,column)->isWhite)
            {
                 if(this->getFieldAt(row, column)->isThreatendForWhite)
                 {
                     this->unthreaten();
                     return true;
                 }
            }
        }
    }
    this->unthreaten();
    return false;
}

bool Chess::isBlackCheck()
{
    this->threaten();
    for(int row=1; row<9; row++)
    {
        for(int column=1; column<9; column++)
        {
            if(this->getFieldAt(row, column)->figureType==6 && this->getFieldAt(row, column)->isWhite==false)
            {
                 if(this->getFieldAt(row, column)->isThreatendForBlack)
                 {
                     this->unthreaten();
                     return true;
                 }
            }
        }
    }
    this->unthreaten();
    return false;
}


bool Chess::isValidTurnForWhite(Field *start, Field *destination)
{
    // save actal status variables
    bool startIsWhite = start->isWhite;
    int startFigureType = start->figureType;
    bool destinationIsWhite = destination->isWhite;
    int destinationFigureType = destination->figureType;

    //make normal turn
    destination->isWhite = start->isWhite;
    destination->figureType = start->figureType;
    start->figureType = 0;

    bool validTurn = false;
    //check if is valid turn
    if(this->isWhiteCheck())
    {
        validTurn = false;
    }
    else {
        validTurn = true;
    }

    //back to start status
    start->isWhite = startIsWhite;
    start->figureType = startFigureType;
    destination->isWhite = destinationIsWhite;
    destination->figureType = destinationFigureType;


    //return result
    return validTurn;
}

bool Chess::isValidTurnForBlack(Field *start, Field *destination)
{
    // save actal status variables
    bool startIsWhite = start->isWhite;
    int startFigureType = start->figureType;
    bool destinationIsWhite = destination->isWhite;
    int destinationFigureType = destination->figureType;

    //make normal turn
    destination->isWhite = start->isWhite;
    destination->figureType = start->figureType;
    start->figureType = 0;

    bool validTurn = false;
    //check if is valid turn
    if(this->isBlackCheck())
    {
        validTurn = false;
    }
    else {
        validTurn = true;
    }

    //back to start status
    start->isWhite = startIsWhite;
    start->figureType = startFigureType;
    destination->isWhite = destinationIsWhite;
    destination->figureType = destinationFigureType;


    //return result
    return validTurn;
}

#endif // CHESS_CPP
