 #ifndef RECEIVER_CPP
#define RECEIVER_CPP

#include "receiver.h"
#include "chess.h"
#include "field.h"


Receiver::Receiver(Chess * chess)
{
    this->chess = chess;
}

Receiver::~Receiver()
{

}


void Receiver::receive(Field * field)
{
    //set lastTurn
    this->chess->lastTurnLayout->lastTurn->setText(this->getTurnString(this->chess->clickedField, field));


    // castlings
    if(this->chess->castlingfield!= nullptr && this->chess->castlingfield == field)
    {
       if(field->gridColumn==7)
       {
           // short white castling
           if(field->isWhite)
           {
               this->chess->getFieldAt(8,8)->figureType=0;
               this->chess->getFieldAt(8,6)->figureType=2;
               this->chess->getFieldAt(8,6)->isWhite=true;
               this->chess->shortWhiteCastling = false;
               this->chess->longWhiteCastling = false;
           }
           // short black castling
           else
           {
               this->chess->getFieldAt(1,8)->figureType=0;
               this->chess->getFieldAt(1,6)->figureType=2;
               this->chess->getFieldAt(1,6)->isWhite=true;
               this->chess->shortBlackCastling = false;
               this->chess->longBlackCastling = false;
           }
           this->chess->lastTurnLayout->lastTurn->setText("0-0");
       }
       if(field->gridColumn==3)
       {
           //long white castling
           if(field->isWhite)
           {
               this->chess->getFieldAt(8,1)->figureType=0;
               this->chess->getFieldAt(8,4)->figureType=2;
               this->chess->getFieldAt(8,4)->isWhite=true;
               this->chess->shortWhiteCastling = false;
               this->chess->longWhiteCastling = false;
           }
           // long black castling
           else
           {
               this->chess->getFieldAt(1,1)->figureType=0;
               this->chess->getFieldAt(1,4)->figureType=2;
               this->chess->getFieldAt(1,4)->isWhite=true;
               this->chess->shortBlackCastling = false;
               this->chess->longBlackCastling = false;
           }
           this->chess->lastTurnLayout->lastTurn->setText("0-0-0");
       }

    }
    this->chess->castlingfield = nullptr;


    // defeat enPasset
    if(this->chess->enPasset==true && this->chess->enPassetField!=nullptr && field == this->chess->enPassetField)
    {
        if(this->chess->isWhitesTurn )
        {
            QString enPassetTurn= "";
            enPassetTurn = this->chess->clickedField->assosiatedFieldButton->text() + this->getCoordinates(this->chess->clickedField);
            enPassetTurn = enPassetTurn + "\u21D2";
            enPassetTurn = enPassetTurn + this->chess->getFieldAt(this->chess->enPassetField->gridRow+1, this->chess->enPassetField->gridColumn)->assosiatedFieldButton->text();
            enPassetTurn = enPassetTurn + this->getCoordinates(field) + " e.p.";
            this->chess->lastTurnLayout->lastTurn->setText(enPassetTurn);

            this->chess->defeatedBlackFiguresGridLayout->addFigure(this->chess->getFieldAt(this->chess->enPassetField->gridRow+1, this->chess->enPassetField->gridColumn));
            this->chess->getFieldAt(this->chess->enPassetField->gridRow+1, this->chess->enPassetField->gridColumn)->figureType = 0;
        }
        else
        {
            QString enPassetTurn = "";
            enPassetTurn = this->chess->clickedField->assosiatedFieldButton->text() + this->getCoordinates(this->chess->clickedField);
            enPassetTurn = enPassetTurn + "\u21D2";
            enPassetTurn = enPassetTurn + this->chess->getFieldAt(this->chess->enPassetField->gridRow-1, this->chess->enPassetField->gridColumn)->assosiatedFieldButton->text();
            enPassetTurn = enPassetTurn + this->getCoordinates(field) + " e.p.";
            this->chess->lastTurnLayout->lastTurn->setText(enPassetTurn);

            this->chess->defeatedWhiteFiguresGridLayout->addFigure(this->chess->getFieldAt(this->chess->enPassetField->gridRow-1, this->chess->enPassetField->gridColumn));
            this->chess->getFieldAt(this->chess->enPassetField->gridRow-1, this->chess->enPassetField->gridColumn)->figureType = 0;
        }


    }
    // defeat the figure
    else if(field->figureType!=0)
    {
         if(field->isWhite)
         {
             this->chess->defeatedWhiteFiguresGridLayout->addFigure(field);
         }
         else
         {
              this->chess->defeatedBlackFiguresGridLayout->addFigure(field);
         }
    }


    // recieved Field will become values of old field
    field->figureType = this->chess->clickedField->figureType;
    field->isWhite = this->chess->clickedField->isWhite;

    // disable castlings
    if(field->figureType==6)
    {
        if(field->isWhite)
        {
            this->chess->shortWhiteCastling = false;
            this->chess->longWhiteCastling = false;
        }
        else
        {
            this->chess->shortBlackCastling = false;
            this->chess->longBlackCastling = false;
        }
    }
    if(field->figureType==2)
    {
        if(field->isWhite && field->gridColumn==8)
        {
            this->chess->shortWhiteCastling = false;
        }
        else if(field->isWhite && field->gridColumn==1)
        {
            this->chess->longWhiteCastling = false;
        }
        else if(field->isWhite==false && field->gridColumn==8)
        {
            this->chess->shortBlackCastling = false;
        }
        else if(field->isWhite==false && field->gridColumn==1)
        {
            this->chess->longBlackCastling = false;
        }
    }

    // old field become empty
    this->chess->clickedField->figureType = 0;

    //trigger en passet
    if(field->figureType==1 && ((field->gridRow-2==this->chess->clickedField->gridRow) || (field->gridRow+2==this->chess->clickedField->gridRow)))
    {
        this->chess->enPasset = true;
        if(field->isWhite)
        {
            this->chess->enPassetField = this->chess->getFieldAt(field->gridRow+1, field->gridColumn);
        }
        else
        {
            this->chess->enPassetField = this->chess->getFieldAt(field->gridRow-1, field->gridColumn);
        }
    }
    else
    {
        this->chess->enPasset = false;
        this->chess->enPassetField = nullptr;
    }


    //change Pawn mechanic
    if((field->gridRow==1 && field->figureType==1) || (field->gridRow==8 && field->figureType==1))
    {
        this->chess->pawnChangeLayout->toPawn = field;
        //disable chess grid
        for(int i=0; i<64; i++)
        {
            Field * actualField = this->chess->fieldList->at(i);
            // normal setting
            QString fieldButtonStyle = "padding:0px; border-radius: 0px; font-size: " + QString::number(this->chess->fontSize) + "px;";
            // background color
            if(((actualField->gridColumn+actualField->gridRow)%2) == 0)
            {
                fieldButtonStyle = fieldButtonStyle + "background-color: #5A4C3D;";
            }
            else
            {
                fieldButtonStyle = fieldButtonStyle + "background-color: #CCC1BA;";
            }
            if (field!=actualField)
            {
                actualField->assosiatedFieldButton->setStyleSheet(fieldButtonStyle);
            }
            //disable buttons
            actualField->assosiatedFieldButton->disconnect(SIGNAL(clicked()));
        }

        // show pawnChange Layout with correct button-background an figure-color
        if((field->gridRow + field->gridColumn) % 2 == 0)
        {
            if(field->isWhite)
            {
                this->chess->pawnChangeLayout->initialiseWhiteFigures("#edb99a");
            }
            else
            {
                this->chess->pawnChangeLayout->initialiseBlackFigures("#edb99a");
            }

        }
        else
        {
            if(field->isWhite)
            {
                  this->chess->pawnChangeLayout->initialiseWhiteFigures("#954D03;");
            }
            else
            {
                  this->chess->pawnChangeLayout->initialiseBlackFigures("#954D03;");
            }

        }


        // change pawn will called from Buttons on pawnChangeLayout
    }

    //end turn
    this->chess->unselectGrid();
    this->chess->updateGrid();
    this->chess->isWhitesTurn = !this->chess->isWhitesTurn;
    if(this->chess->isWhitesTurn)
    {
      this->chess->turnInformationGridLayout->isWhitesTurnRadioButton->setChecked(true);
    }
    else
    {
      this->chess->turnInformationGridLayout->isBlacksTurnRadioButton->setChecked(true);
    }

    if(this->chess->isWhitesTurn)
    {
        if(this->chess->isWhiteCheck()==true)
        {
            QMessageBox * msgBox = new QMessageBox();
            msgBox->setText("Weißer König im Schach!");
            msgBox->setModal(true);
            msgBox->exec();
        }

    }
    else
    {
        if(this->chess->isBlackCheck()==true)
        {
            QMessageBox *  msgBox = new QMessageBox();
            msgBox->setText("Schwarzer König im Schach!");
            msgBox->setModal(true);
            msgBox->exec();
        }
    }

}

QString Receiver::getTurnString(Field *oldField, Field *newField)
{
    QString turnString;
    turnString = turnString + oldField->assosiatedFieldButton->text();
    turnString = turnString + this->getCoordinates(oldField);
    turnString = turnString + "\u21D2";
    turnString = turnString + newField->assosiatedFieldButton->text();
    turnString = turnString + this->getCoordinates(newField);

    return turnString;
}

QString Receiver::getCoordinates(Field *field)
{
    QString coordinates;
    if(field->gridColumn == 1)
    {
        coordinates = "A";
    }
    else if (field->gridColumn == 2)
    {
        coordinates = "B";
    }
    else if (field->gridColumn == 3)
    {
        coordinates = "C";
    }
    else if (field->gridColumn == 4)
    {
        coordinates = "D";
    }
    else if (field->gridColumn == 5)
    {
        coordinates = "E";
    }
    else if (field->gridColumn == 6)
    {
        coordinates = "F";
    }
    else if (field->gridColumn == 7)
    {
        coordinates = "G";
    }
    else if (field->gridColumn == 8)
    {
        coordinates = "H";
    }
    coordinates = coordinates + QString::number(9-field->gridRow);

    return  coordinates;
}

void Receiver::changePawn(Field * field)
{
    field->figureType = this->chess->pawnChangeLayout->toFigureTyp;
    this->chess->updateGrid();

    //reset grid colors and reactivate Buttons
    for(int i=0; i<64; i++)
    {
        Field * actualField = this->chess->fieldList->at(i);
        // normal setting
        QString fieldButtonStyle = "padding:0px; border-radius: 0px; font-size: " + QString::number(this->chess->fontSize) + "px;";
        // background color
        if(((actualField->gridColumn+actualField->gridRow)%2) == 0)
        {
            fieldButtonStyle = fieldButtonStyle + "background-color: #edb99a;";
        }
        else
        {
            fieldButtonStyle = fieldButtonStyle + "background-color: #954D03;";
        }
        if (field!=actualField)
        {
            actualField->assosiatedFieldButton->setStyleSheet(fieldButtonStyle);
        }
        actualField->assosiatedFieldButton->connect(actualField->assosiatedFieldButton, SIGNAL(clicked()), actualField->assosiatedFieldButton, SLOT(click()));
    }

}

#endif // RECEIVER_CPP
