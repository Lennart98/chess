#ifndef KING_CPP
#define KING_CPP

#include "king.h"
#include "chess.h"
#include "field.h"

#include <iostream>

King::King(Chess * chess)
{
    this->chess = chess;
}

King::~King()
{

}

void King::turn(Field *field)
{
    //mark threaten fields
    this->chess->threaten();

    // 8 fields, one in each direction

    //up
    if(field->gridRow-1>0 && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->figureType==0 ||this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn));
        }
    }

    //up-right
    if(field->gridRow-1>0 && field->gridColumn+1<9 && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->figureType==0 ||this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1));
        }
    }

    //right
    if(field->gridColumn+1<9 && (this->chess->getFieldAt(field->gridRow, field->gridColumn+1)->figureType==0 ||this->chess->getFieldAt(field->gridRow, field->gridColumn+1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow, field->gridColumn+1)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn+1));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow, field->gridColumn+1)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn+1));
        }
    }

    //down-right
    if(field->gridRow+1<9 && field->gridColumn+1<9 && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->figureType==0 ||this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1));
        }
    }

    //down
    if(field->gridRow+1<9 && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->figureType==0 ||this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn));
        }
    }

    //down-left
    if(field->gridRow+1<9 && field->gridColumn-1>0 && (this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->figureType==0 ||this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1));
        }
    }

    //left
    if(field->gridColumn-1>0 && (this->chess->getFieldAt(field->gridRow, field->gridColumn-1)->figureType==0 ||this->chess->getFieldAt(field->gridRow, field->gridColumn-1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow, field->gridColumn-1)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn-1));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow, field->gridColumn-1)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow, field->gridColumn-1));
        }
    }

    //up-left
    if(field->gridRow-1>0 && field->gridColumn-1>0 && (this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->figureType==0 ||this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->isWhite!=field->isWhite))
    {
        if(field->isWhite && this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->isThreatendForWhite==false)
        {
           this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1));
        }
        else if(field->isWhite==false && this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->isThreatendForBlack==false)
        {
            this->chess->selectField(this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1));
        }
    }


    // castlings
    // short white castling
    if(field->isWhite && this->chess->shortWhiteCastling==true)
    {
        // check if fields are empty
        if(this->chess->getFieldAt(8, 6)->figureType==0 && this->chess->getFieldAt(8, 7)->figureType==0){
            this->chess->selectField(this->chess->getFieldAt(8, 7));
            this->chess->castlingfield = this->chess->getFieldAt(8, 7);
        }
    }
    // long white castling
    if(field->isWhite && this->chess->longWhiteCastling==true)
    {

        // check if fields are empty
        if(this->chess->getFieldAt(8, 4)->figureType==0 && this->chess->getFieldAt(8, 3)->figureType==0 && this->chess->getFieldAt(8, 2)->figureType==0)
        {
            this->chess->selectField(this->chess->getFieldAt(8, 3));
            this->chess->castlingfield = this->chess->getFieldAt(8, 3);
        }
    }
    // short black castling
    if(field->isWhite==false && this->chess->shortBlackCastling==true)
    {
        // check if fields are empty
        if(this->chess->getFieldAt(1, 6)->figureType==0 && this->chess->getFieldAt(1, 7)->figureType==0){
            this->chess->selectField(this->chess->getFieldAt(1, 7));
            this->chess->castlingfield = this->chess->getFieldAt(1, 7);
        }
    }
    // long black castling
    if(field->isWhite==false && this->chess->longBlackCastling==true)
    {

        // check if fields are empty
        if(this->chess->getFieldAt(1, 4)->figureType==0 && this->chess->getFieldAt(1, 3)->figureType==0 && this->chess->getFieldAt(1, 2)->figureType==0)
        {
            this->chess->selectField(this->chess->getFieldAt(1, 3));
            this->chess->castlingfield = this->chess->getFieldAt(1, 3);
        }
    }

    this->chess->unthreaten();
}


void King::threat(Field *field)
{
    //up
    if(field->gridRow-1>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow-1, field->gridColumn)->isThreatendForWhite = true;
        }
    }

    //up-right
    if(field->gridRow-1>0 && field->gridColumn+1<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow-1, field->gridColumn+1)->isThreatendForWhite = true;
        }
    }

    //right
    if(field->gridColumn+1<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow, field->gridColumn+1)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow, field->gridColumn+1)->isThreatendForWhite = true;
        }
    }

    //down-right
    if(field->gridRow+1<9 && field->gridColumn+1<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow+1, field->gridColumn+1)->isThreatendForWhite = true;
        }
    }

    //down
    if(field->gridRow+1<9)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow+1, field->gridColumn)->isThreatendForWhite = true;
        }
    }

    //down-left
    if(field->gridRow+1<9 && field->gridColumn-1>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow+1, field->gridColumn-1)->isThreatendForWhite = true;
        }
    }

    //left
    if(field->gridColumn-1>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow, field->gridColumn-1)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow, field->gridColumn-1)->isThreatendForWhite = true;
        }
    }

    //up-left
    if(field->gridRow-1>0 && field->gridColumn-1>0)
    {
        if(field->isWhite)
        {
            this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->isThreatendForBlack = true;
        }
        else
        {
           this->chess->getFieldAt(field->gridRow-1, field->gridColumn-1)->isThreatendForWhite = true;
        }
    }


}

#endif
