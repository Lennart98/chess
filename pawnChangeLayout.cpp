#ifndef PAWNCHANGELAYOUT_CPP
#define PAWNCHANGELAYOUT_CPP

#include "pawnChangeLayout.h"
#include "chess.h"

PawnChangeLayout::PawnChangeLayout(Chess * chess)
{
    this->chess = chess;

    QString textSize = "font-size: " + QString::number(this->chess->fontSize / 3) + "px;";
    this->pawnChangeLabel = new QLabel("Zielfigur:");
    this->pawnChangeLabel->setStyleSheet(textSize);
    this->addWidget(this->pawnChangeLabel, 0, 0, Qt::AlignCenter);
    this->pawnChangeLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);


    this->buttonStyle = "padding:0px; border-radius: 0px; font-size: " + QString::number(this->chess->fontSize) + "px;";
    this->toQueen = new QPushButton();
    this->toQueen->setStyleSheet(buttonStyle);
    this->toQueen->setMinimumSize(this->chess->gridSize,this->chess->gridSize);
    this->toQueen->setMaximumSize(this->toQueen->minimumSize());
    this->connect(this->toQueen, SIGNAL(clicked()), this, SLOT(setQueen()));
    this->addWidget(this->toQueen, 1, 0, Qt::AlignCenter);

    this->toRook = new QPushButton();
    this->toRook->setStyleSheet(buttonStyle);
    this->toRook->setMinimumSize(this->chess->gridSize,this->chess->gridSize);
    this->toRook->setMaximumSize(this->toRook->minimumSize());
    this->connect(this->toRook, SIGNAL(clicked()), this, SLOT(setRook()));
    this->addWidget(this->toRook, 2, 0, Qt::AlignCenter);

    this->toBishop = new QPushButton();
    this->toBishop->setStyleSheet(buttonStyle);
    this->toBishop->setMinimumSize(this->chess->gridSize,this->chess->gridSize);
    this->toBishop->setMaximumSize(this->toBishop->minimumSize());
    this->connect(this->toBishop, SIGNAL(clicked()), this, SLOT(setBishop()));
    this->addWidget(this->toBishop, 3, 0, Qt::AlignCenter);

    this->toKnight = new QPushButton();
    this->toKnight->setStyleSheet(buttonStyle);
    this->toKnight->setMinimumSize(this->chess->gridSize,this->chess->gridSize);
    this->toKnight->setMaximumSize(this->toKnight->minimumSize());
    this->connect(this->toKnight, SIGNAL(clicked()), this, SLOT(setKnight()));
    this->addWidget(this->toKnight, 4, 0, Qt::AlignCenter);
    this->setVisible(false);
    this->setSpacing(this->chess->gridSize/80);

}


PawnChangeLayout::~PawnChangeLayout()
{
    delete this->pawnChangeLabel;
    delete this->toQueen;
    delete this->toBishop;
    delete this->toKnight;
}


void PawnChangeLayout::setVisible(bool value)
{
     this->pawnChangeLabel->setVisible(value);
     this->toQueen->setVisible(value);
     this->toRook->setVisible(value);
     this->toBishop->setVisible(value);
     this->toKnight->setVisible(value);

}

void PawnChangeLayout::initialiseWhiteFigures(QString background)
{
    this->toQueen->setText("\u2655");
    this->toRook->setText("\u2656");
    this->toBishop->setText("\u2657");
    this->toKnight->setText("\u2658");
    this->setBackgound(background);
    this->setVisible(true);
}

void PawnChangeLayout::initialiseBlackFigures(QString background)
{
    this->toQueen->setText("\u265B");
    this->toRook->setText("\u265C");
    this->toBishop->setText("\u265D");
    this->toKnight->setText("\u265E");
    this->setBackgound(background);
    this->setVisible(true);
}

void PawnChangeLayout::setBackgound(QString background)
{
    this->toQueen->setStyleSheet(this->buttonStyle + "background-color: " + background + ";");
    this->toRook->setStyleSheet(this->buttonStyle + "background-color: " + background + ";");
    this->toBishop->setStyleSheet(this->buttonStyle + "background-color: " + background + ";");
    this->toKnight->setStyleSheet(this->buttonStyle + "background-color: " + background + ";");
}

void PawnChangeLayout::setQueen()
{
    this->toFigureTyp = 5;
    this->chess->receiver->changePawn(this->toPawn);
    this->setVisible(false);
    this->toPawn = nullptr;
}

void PawnChangeLayout::setRook()
{
    this->toFigureTyp = 2;
    this->chess->receiver->changePawn(this->toPawn);
    this->setVisible(false);
    this->toPawn = nullptr;
}

void PawnChangeLayout::setBishop()
{
    this->toFigureTyp = 4;
    this->chess->receiver->changePawn(this->toPawn);
    this->setVisible(false);
    this->toPawn = nullptr;
}

void PawnChangeLayout::setKnight()
{
    this->toFigureTyp = 3;
    this->chess->receiver->changePawn(this->toPawn);
    this->setVisible(false);
    this->toPawn = nullptr;
}

#endif
