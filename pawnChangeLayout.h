#ifndef PAWNCHANGELAYOUT_H
#define PAWNCHANGELAYOUT_H

#include <QGridLayout>
#include <QPushButton>
#include <QLabel>

class Chess;
class Field;

class PawnChangeLayout : public QGridLayout
{
Q_OBJECT
public:
    PawnChangeLayout(Chess * chess);
    ~PawnChangeLayout();

    Chess * chess;

    QLabel * pawnChangeLabel;

    QString buttonStyle;

    QPushButton *toQueen;
    QPushButton *toRook;
    QPushButton *toBishop;
    QPushButton *toKnight;

    Field * toPawn;

    int toFigureTyp = 0;

    void initialiseWhiteFigures(QString);
    void initialiseBlackFigures(QString);
    void setBackgound(QString);
    void setVisible(bool);

public slots:
    void setQueen();
    void setRook();
    void setBishop();
    void setKnight();
};


#endif // PAWNCHANGELAYOUT_H
