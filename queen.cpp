#ifndef QUEEN_CPP
#define QUEEN_CPP

#include "queen.h"
#include "chess.h"
#include "field.h"

Queen::Queen(Chess * chess)
{
    this->chess = chess;
}

Queen::~Queen()
{

}

void Queen::turn(Field *field)
{
    this->chess->bishop->turn(field);
    this->chess->rook->turn(field);
}

void Queen::threat(Field *field)
{
    this->chess->bishop->threat(field);
    this->chess->rook->threat(field);
}

#endif
