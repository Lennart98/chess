#ifndef FIELDBUTTON_CPP
#define FIELDBUTTON_CPP

#include "fieldButton.h"
#include "chess.h"

FieldButton::FieldButton(Chess * chess) : QPushButton()
{
    this->chess = chess;
    connect(this, SIGNAL(clicked()), this, SLOT(click()));


}

FieldButton::~FieldButton()
{

}

void FieldButton::click()
{
    this->chess->fieldClicked(this->gridRow, this->gridColumn);
}

#endif // FIELDBUTTON_CPP
