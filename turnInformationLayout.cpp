#ifndef TURNINFORMATIONLAYOUT_CPP
#define TURNINFORMATIONLAYOUT_CPP

#include "turnInformationLayout.h"
#include "chess.h"

TurnInformationLayout::TurnInformationLayout(Chess * chess)
{
    this->chess = chess;



    QString textSize = "font-size: " + QString::number(this->chess->fontSize / 3) + "px;";
    this->isWhitesTurnRadioButton = new QRadioButton("weiß");
    this->isWhitesTurnRadioButton->setStyleSheet(textSize);
    this->isWhitesTurnRadioButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    this->isWhitesTurnRadioButton->setEnabled(false);
    connect(this->isWhitesTurnRadioButton, SIGNAL(toggled(bool)), this, SLOT(toggleWhite()));
    this->isBlacksTurnRadioButton = new QRadioButton("schwarz");
    this->isBlacksTurnRadioButton->setStyleSheet(textSize);
    this->isBlacksTurnRadioButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    this->isBlacksTurnRadioButton->setEnabled(false);
    connect(this->isBlacksTurnRadioButton, SIGNAL(toggled(bool)), this, SLOT(toggleBlack()));
    this->turnButtonGroup = new QButtonGroup();
    this->turnButtonGroup->addButton(this->isWhitesTurnRadioButton);
    this->turnButtonGroup->addButton(this->isBlacksTurnRadioButton);
    this->isWhitesTurnRadioButton->setChecked(this->chess->isWhitesTurn);

    this->turnLabel = new QLabel("Zug:");
    this->turnLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    this->turnLabel->setStyleSheet(textSize);
    this->addWidget(this->turnLabel, 0, 1);
    this->addWidget(this->isWhitesTurnRadioButton, 1, 1);
    this->addWidget(this->isBlacksTurnRadioButton, 2, 1);

    this->bottomSpace = new QSpacerItem(0,this->chess->gridSize/2);
    this->addItem(this->bottomSpace, 3,1);

}

TurnInformationLayout::~TurnInformationLayout()
{
    delete this->turnButtonGroup;
    delete this->turnLabel;
    delete this->isWhitesTurnRadioButton;
    delete this->isBlacksTurnRadioButton;
    this->removeItem(this->bottomSpace);
    delete this->bottomSpace;
}

void TurnInformationLayout::toggleWhite()
{
    this->chess->isWhitesTurn = true;
}
void TurnInformationLayout::toggleBlack()
{
    this->chess->isWhitesTurn = false;
}

#endif
