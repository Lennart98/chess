#ifndef DEFEATEDFIGUREGRIDLAYOUT_H
#define DEFEATEDFIGUREGRIDLAYOUT_H

#include <QGridLayout>
#include <QList>
#include <QLabel>

class Chess;
class Field;

class DefeatedFigureGridLayout : public QGridLayout
{
Q_OBJECT
public:
    DefeatedFigureGridLayout(Chess * chess);
    ~DefeatedFigureGridLayout();
    Chess * chess;
    int index = 0;
    QList<QLabel *> * figuresList;
    void addFigure(Field * field);
};


#endif // DEFEATEDFIGUREGRIDLAYOUT_H
