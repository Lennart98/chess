#ifndef KNIGHT_H
#define KNIGHT_H

class Chess;
class Field;

class Knight
{
public:
    Knight(Chess * chess);
    ~Knight();

    Chess * chess;

    void turn(Field * field);
    void threat(Field * field);
};

#endif // KNIGHT_H
