#ifndef DEFEATEDGRIDLAYOUT_CPP
#define DEFEATEDGRIDLAYOUT_CPP

#include "chess.h"
#include "defeatedFigureGridLayout.h"
#include "field.h"

DefeatedFigureGridLayout::DefeatedFigureGridLayout(Chess * chess)
{
    this->chess = chess;
    this->figuresList = new QList<QLabel *>();
    QString style = "font-size: " + QString::number(this->chess->fontSize) + "px;";

    int gridRow = 0;
    int gridColumn = 0;
    for(int i = 0; i<8; i++)
    {
        QLabel *actualFigure = new QLabel();
        actualFigure->setMinimumSize(this->chess->gridSize, this->chess->gridSize);
        actualFigure->setMaximumSize(actualFigure->minimumSize());
        actualFigure->setStyleSheet(style);
        figuresList->append(actualFigure);
        this->addWidget(actualFigure, gridRow, gridColumn);
        gridColumn = gridColumn + 1;
        if(gridColumn==2)
        {
           gridColumn = 0;
           gridRow = gridRow + 1;
        }
    }
}

DefeatedFigureGridLayout::~DefeatedFigureGridLayout()
{
    for(int i = 0; i<8; i++)
    {
        delete this->figuresList->at(i);
    }
    delete this->figuresList;
}

void DefeatedFigureGridLayout::addFigure(Field * field)
{
    this->figuresList->at(this->index)->setText(field->assosiatedFieldButton->text());
    this->index = this->index + 1;
}

#endif
