#ifndef KING_H
#define KING_H

class Field;
class Chess;

class King
{
public:
    King(Chess * chess);
    ~King();

    Chess * chess;

    void turn(Field * field);
    void threat(Field * field);

};


#endif // KING_H
